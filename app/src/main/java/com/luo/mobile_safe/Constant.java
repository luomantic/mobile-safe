package com.luo.mobile_safe;

public class Constant {
    public static final String NAMESPACE = "http://schemas.android.com/apk/res-auto";

    public static String UPDATE_URL = "https://raw.githubusercontent.com/luomantic/mobile-safe/master/note/";
    public static String DOWNLOAD_URL = "https://raw.githubusercontent.com/luomantic/mobile-safe/master/note/mobile-safev2.0.apk";

    /* ------------------ SPUtils键值对的名称 ------------------ */
    public static String OPEN_UPDATE = "open_update"; // 是否升级
    public static String PASSWORD = "password"; // 手机防盗页密码
    public static String IS_CONFIGED = "config"; // 手机防盗页，设置向导
}
